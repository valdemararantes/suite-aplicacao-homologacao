package br.com.galgo.testes.suite;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.annotations.BeforeSuite;

/**
 * Created by valdemar.arantes on 05/06/2015.
 */
public class SuiteAplicacaoHomologacaoListener implements ISuiteListener {

    private static final Logger log = LoggerFactory.getLogger(SuiteAplicacaoHomologacaoListener.class);

    @BeforeSuite
    public void beforeSuite() {
        log.debug("Antes da suíte");
    }

    /**
     * This method is invoked after the SuiteRunner has run all
     * the test suites.
     *
     * @param suite
     */
    public void onFinish(ISuite suite) {
        //@formatter:off
        log.info("\n"
            + "****************************************************************************************\n"
            + "Fim da suíte {}\n"
            + "****************************************************************************************"
            + "\n", suite.getName());
        //@formatter:on
        TelaGalgo.fecharBrowser();
    }

    /**
     * This method is invoked before the SuiteRunner starts.
     *
     * @param suite
     */
    public void onStart(ISuite suite) {
        //@formatter:off
        log.info("\n"
            + "****************************************************************************************\n"
            + "Iniciando a suíte {}\n"
            + "****************************************************************************************"
            + "\n",
            suite.getName());
        //@formatter:on
        SuiteUtils.configurarSuiteDefault(Ambiente.HOMOLOGACAO, "Aplicação");
    }


}

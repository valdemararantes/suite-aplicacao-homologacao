package br.com.galgo.testes.suite;

import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;

/**
 * Created by valdemar.arantes on 05/06/2015.
 */
public class SuiteAplicacaoListener extends br.com.galgo.testes.recursos_comuns.utils.SuiteAplicacaoListener {

    @Override
    protected Ambiente getAmbiente() {
        return Ambiente.HOMOLOGACAO;
    }
}
